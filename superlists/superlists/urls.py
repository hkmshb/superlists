from django.conf.urls import patterns, include, url



urlpatterns = patterns('',
    url(r'^$', 'lists.views.index', name='home'),
    url(r'^lists/', include('lists.urls')),
    # url(r'^admin/', include(admin.site.urls)),
)
